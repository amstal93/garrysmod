# [shatteredscreen/garrysmod](https://gitlab.com/ShatteredScreen/docker/garrysmod)
A dockerized Garry's Mod Dedicated Server.

After struggling to find a docker image for a Garry's Mod server that fit my needs, let alone that worked, I gave up and [made my own](https://xkcd.com/927/) using the [cm2/steamcmd](https://github.com/CM2Walki/steamcmd) as a base image.

## Usage

The quickest way to spin up an instance of the image is like so:
```bash
$ docker run -d \
  --name=garrysmod \
  -v </path/to/steam/games>:/home/steam/games \
  -p 27015/tcp \
  -p 27015/udp \
  -p 27020/udp \
  -p 27005/udp \
  -p 26900/udp \
  shatteredscreen/garrysmod
```

For more complete examples using `docker compose`, please refere to the [/examples]() directory or the project.

## Parameters
|Parameter                            |Optional |Function                                                                                                                                                                                           |
|-------------------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|`-p 27015/tcp`                       |✖       |Game Transmission, Pings and [RCON](https://developer.valvesoftware.com/wiki/Source_Dedicated_Server)                                                                                              |
|`-p 27015/udp`                       |✖       |Game Transmission, Pings and [RCON](https://developer.valvesoftware.com/wiki/Source_Dedicated_Server)                                                                                              |
|`-p 27020/udp`                       |✖       |SourceTV Transmission                                                                                                                                                                              |
|`-p 27005/udp`                       |✖       |Client Port                                                                                                                                                                                        |
|`-p 26900/udp`                       |✖       |Steam Port, outgoing                                                                                                                                                                               |
|`-e HOSTNAME="Cool Server Name"`     |✔       |Hostname of the server that'll show up in the server browser                                                                                                                                       |
|`-e PASSWORD=<PASSWORD>`             |✔       |Password required to join the server                                                                                                                                                               |
|`-e GAMEMODE=sandbox`                |✔       |Gamemode running on the server                                                                                                                                                                     |
|`-e MAP=gm_construct`                |✔       |Map that the game will startup on                                                                                                                                                                  |
|`-e MAX_PLAYERS=16`                  |✔       |Maximum number of player on the server                                                                                                                                                             |
|`-e WORKSHOP_COLLECTION=2522605046`  |✔       |ID of the workshop collection that will be installed on the server. This can be gotten from the url of the workshop collection `https://steamcommunity.com/sharedfiles/filedetails/?id=2522605046` |
|`-e GSLT=<TOKEN>`                    |✔       |Steam Game Server Account Management token that can be generated [here](Steam Game Server Account Management) with the App ID `4000`                                                               |
|`-e RCON_PASSWORD=<PASSWORD>`        |✔       |Password used to manage the server through RCON                                                                                                                                                    |
|`-e TICKRATE=66`                     |✔       |Tickrate of the server                                                                                                                                                                             |
|`-e ADDITIONAL_ARGS="+sv_cheats 1"`  |✔       |Any additional command line arguments to add while starting up the server                                                                                                                          |
|`-v /home/steam/games`               |✖       |Directory where steam applications are installed                                                                                                                                                   |

>All environment variables, such as `-e PASSWORD=<PASSWORD>` can also be appended with `_FILE` to read the value from a textfile, like so `-e PASSWORD_FILE=/run/secrets/password.txt`